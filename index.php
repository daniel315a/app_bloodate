<?php

	/**
	 * Constante que determina si se está 
	 * ejecutando en depuración o producción
	 */
	define('DEBUG', true);
	
	include ('autoload.php');

	/*
	 * Entrada el Backend
	 */

  	$controlador = obtenerControlador();

  	if($controlador != false)
  	{
		establecerCabecerasGenerales();

		$rutaControlador = 'Core/' . obtenerNombreArchivo($controlador) . '.php';

		if(is_readable($rutaControlador))
		{
			call_user_func($controlador . '::' . strtolower($_SERVER['REQUEST_METHOD']));
		}

  	}

?>