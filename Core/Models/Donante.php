<?php namespace Models;

	/**
	 * Contiene los métodos, atributos y propiedades,
	 * relacionados con la tabla donantes de la base
	 * de datos
	 */
	class Donante extends Usuario
	{

		private $id;
		public $tipo_sangre;

		function __construct()
		{
			# code...
		}

		/*
		 * Encapsulamiento
		 */

		public function setTipoSangre($id_tipo_sangre)
		{
			$this->tipo_sangre = new TipoSangre();
			$this->tipo_sangre->consultarPorId($id_tipo_sangre);
		} 

		public function setIdUsuario($id)
		{
			parent::setId($id);
		}

		/*
		 * Métodos
		 */

		public function crear()
		{
			$conexion = new \Conexion();
			$this->token_seguridad = $this->getToken();
			$id_usuario = parent::getId();

			$sql = "
					INSERT INTO donantes
						(usuario,
						tipo_sangre)
					VALUES
					(
						{$id_usuario},
						{$this->tipo_sangre->getId()}
					);
					";
					
			$resultado = $conexion->execCommand($sql);
			$respuesta = null;

			if($resultado == true)
			{
				$this->id = $conexion->getIdInsertado();

				$respuesta = new \Respuesta(
					[
						'resultado' => true,
						'datos' => $this
					]
				);
			}
			else
			{
				$respuesta = \Respuesta::obtenerDefault();
			}

			return $respuesta;
		}

		public function consultarTodos()
		{
			$sql = "SELECT nombre_acceso,
							token_seguridad,
							numero_documento,
							nombres,
							apellidos,
							activo,
							tipos_sangre.id AS id_tipo_sangre,
							tipos_sangre.nombre AS tipo_sangre
					FROM usuarios
					LEFT JOIN donantes
						ON donantes.usuario = usuarios.id
					LEFT JOIN tipos_sangre
						ON donantes.tipo_sangre = tipos_sangre.id
					WHERE donantes.usuario IS NOT NULL";

			$conexion = new \Conexion();
			$data = $conexion->getData($sql);
			$respuesta = \Respuesta::obtenerDefault();

			if($conexion->getCantidadRegistros() > 0)
			{
				$respuesta = new \Respuesta([
					'resultado' => true,
					'datos' => $data
				]);
			}

			return $respuesta;
		}

	}

?>