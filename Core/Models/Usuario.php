<?php namespace Models;

	/**
	 * Contiene los métodos, atributos y propiedades
	 * relacionados con la tabla usuarios de la DB
	 */
	class Usuario
	{
		
		private $id;
		public $nombre_acceso;
		private $contrasenna;
		public $token_seguridad;

		function __construct()
		{
		}

		/*
		 * Encapsulamiento
		 */

		public function getId()
		{
			return $this->id;
		}

		protected function setId($id)
		{
			$this->id = $id;
		}

		public function setContrasenna($contrasenna)
		{
			$this->contrasenna = md5($contrasenna);
		}

		/*
		 * Construye y retorna el token de seguridad del objeto actual
		 */
		public function getToken()
		{
			$string_token = $this->nombre_acceso . $this->getId();
			return md5($string_token);
		}

		/*
		 * Métodos
		 */

		public function consultarUsuarios()
		{
			$conexion = new \Conexion();
			
			$sql = "
					SELECT nombre_acceso,
							token_seguridad,
							nombres,
							apellidos,
							activo
					FROM usuarios;
					";

			$data = $conexion->getData($sql);
			$respuesta = null;

			if($conexion->getCantidadRegistros() > 0)
			{
				$respuesta = new \Respuesta(
					[
						'resultado' => true,
						'datos' => $data
					]
				);
			}
			else
			{
				$respuesta = \Respuesta::obtenerDefault();
			}

			return $respuesta;
		}
		
		public function crear()
		{
			$conexion = new \Conexion();
			$this->token_seguridad = $this->getToken();

			$sql = "
					INSERT INTO usuarios
						(nombre_acceso,
						contrasenna,
						token_seguridad,
						numero_documento,
						nombres,
						apellidos)
					VALUES
					(
						'{$this->nombre_acceso}',
						'{$this->contrasenna}',
						'{$this->token_seguridad}',
						'{$this->numero_documento}',
						'{$this->nombres}',
						'{$this->apellidos}'
					);
					";

			$resultado = $conexion->execCommand($sql);
			$respuesta = null;

			if($resultado == true)
			{
				$this->id = $conexion->getIdInsertado();

				$respuesta = new \Respuesta(
					[
						'resultado' => true,
						'datos' => $this
					]
				);
			}
			else
			{
				$respuesta = \Respuesta::obtenerDefault();
			}

			return $respuesta;
		}

		public function login()
		{
			$sql = "SELECT nombre_acceso, 
							token_seguridad, 
							numero_documento, 
							nombres, 
							apellidos, 
							(CASE 
								WHEN donantes.id IS NOT NULL THEN 'donante' 
								WHEN empleados.id IS NOT NULL THEN 'empleado' 
							END) AS tipo,
							donantes.tipo_sangre,
							empleados.telefono
					FROM usuarios
					LEFT JOIN donantes
						ON usuarios.id = donantes.usuario
					LEFT JOIN empleados
						ON usuarios.id = empleados.usuario
					WHERE nombre_acceso = '{$this->nombre_acceso}' AND contrasenna = '{$this->contrasenna}';";

			$conexion = new \Conexion();
			$data = $conexion->getData($sql);
			$respuesta = \Respuesta::obtenerDefault();

			if($conexion->getCantidadRegistros() > 0)
			{
				if($data[0]->tipo_sangre != null)
				{
					$tipo_sangre = new TipoSangre();
					$tipo_sangre->consultarPorId($data[0]->tipo_sangre);
					$data[0]->tipo_sangre = $tipo_sangre;
				}
				
				if($data[0]->telefono == null)
				{
					unset($data[0]->telefono);
				}
				else if($data[0]->tipo_sangre == null)
				{
					unset($data[0]->tipo_sangre);
				}

				$respuesta = new \Respuesta([
					'resultado' => true,
					'datos' => $data
				]);
			}

			return $respuesta;
		}

	}

?>