<?php namespace Models;

	/**
	 * Contiene los métodos atributos y propiedades 
	 * relacionados con la tabla empleados de la DB
	 */
	class Empleado extends Usuario
	{
		
		private $id;
		public $telefono;

		/*
		 * Encapsulamiento
		 */

		public function getId()
		{
			return $this->id;
		}

		public function setIdUsuario($id)
		{
			parent::setId($id);
		}

		function __construct()
		{

		}

		/*
		 * Métodos
		 */

		public function crear()
		{
			$id_usuario = parent::getId();

			$sql = "INSERT INTO empleados 
					(
						usuario,
						telefono
					)
					VALUES
					(
						{$id_usuario},
						'{$this->telefono}'
					);";

			$conexion = new \Conexion();
			$resultado = $conexion->execCommand($sql);
			$respuesta = \Respuesta::obtenerDefault();

			if($resultado == true)
			{
				$respuesta = new \Respuesta([
					'resultado' => $resultado,
					'datos' => $this
				]);
			}

			return $respuesta;
		}

	}

?>