<?php namespace Models;

	/*
	 * Contiene los métodos, atributos y propiedades,
	 * relacionados con la tabla tipos_sangre de la base
	 * de datos
	 */
	class TipoSangre
	{

		public $id;
		public $nombre;

		function __construct()
		{
			# code...
		}

		/*
		 * Encapsulamiento
		 */

		public function getId()
		{
			return $this->id;
		}

		/*
		 * Métodos de lógica
		 */

		public function consultarPorId($id)
		{
			$conexion = new \Conexion();
			$this->id = $id;

			$sql = "SELECT nombre
					FROM tipos_sangre
					WHERE id = {$this->id};";

			$data = $conexion->getData($sql);
			$respuesta = null;

			if($data == true)
			{
				$this->nombre = $data[0]->nombre;

				$respuesta = new \Respuesta(
					[
						'resultado' => true,
						'datos' => $data
					]
				);
			}
			else
			{
				$respuesta = \Respuesta::obtenerDefault();
			}

			return $respuesta;
		}

		public function consultarTodos()
		{
			$conexion = new \Conexion();

			$sql = "SELECT id, nombre
					FROM tipos_sangre
					ORDER BY id ASC;";

			$data = $conexion->getData($sql);
			$respuesta = \Respuesta::obtenerDefault();

			if($data == true)
			{
				$this->nombre = $data[0]->nombre;

				$respuesta = new \Respuesta(
					[
						'resultado' => true,
						'datos' => $data
					]
				);
			}

			return $respuesta;
		}


	}

?>