<?php

	/*
	 * Variables Globales
	 */

	/*
	 * Roles de Usuario
	 */

	define('ADMINISTRADOR', 'administrador');
	define('EMPLEADO', 'empleado');
	define('DONANTE', 'donante');

	/*
	 * Fin Variables Globales
	 */

	/**
	 * Retorna el id de una petición por el método DELETE
	 * las claves siempre van a ser: id y ide
	 */
	function _DELETE($clave = '')
	{
		$arr_url = explode('/', $_SERVER['REQUEST_URI']);
		$id = end($arr_url);

		if(!is_numeric($id))
		{
			$id = 0;
		}

		$parametros = [
			'id' => $id
		];

		if(empty($clave))
		{
			return $parametros;
		}

		if (isset($parametros[$clave]))
		{
			return $parametros[$clave];
		}
		else
		{
			return null;
		}
	}

	/**
	 * Retorna el valor de un parámetro recibiendo la clave
	 * si no se recibe nada, se retorna todo el arreglo de parámetros
	 */
	function _PUT($clave = '')
	{
		$parametros = $GLOBALS['_PUT'];

		if(empty($clave))
		{
			return $parametros;
		}

		if (isset($parametros[$clave]))
		{
			return $parametros[$clave];
		}
		else
		{
			return null;
		}
	}

	/*
	 * Se usa para castear objetos con tipos NO nativos
	 */
	function cast($object, $class_name) 
	{
        if($object === false) return false;

        if(class_exists($class_name)) 
        {
            $ser_object = serialize($object);
            $obj_name_len = strlen(get_class($object));
            $start = $obj_name_len + strlen($obj_name_len) + 6;
            $new_object = 'O:' . strlen($class_name) . ':"' . $class_name . '":';
            $new_object .= substr($ser_object, $start);
            $new_object = unserialize($new_object);
            $graph = new $class_name;
            
            foreach($new_object as $prop => $val) {
                $graph->$prop = $val;
            }

            return $graph;
        } 
        else 
        {
            throw new CoreException(false, "could not find class $class_name for casting in DB::cast");
            return false;
        }
    }

	/**
	 * Retorna un controlador teniendo en cuenta
	 * los parámetros de la URI actual
	 */
	function obtenerControlador()
	{
		$url = explode('?', $_SERVER['REQUEST_URI']);
		$arr_uri = explode('/', $url[0]);
		$controlador = 'Controllers\\' . $arr_uri[2];

		/* Si hay más de \ en la url
		 * no se reconoce la petición
		 */
		if(count($arr_uri) > 5)
		{
			return false;
		}

		/* 
		 * Si la posición 2 de la url (separando por /)
		 * es empty o index.php, no se reconoce la petición
		 */
		return $arr_uri[2] === '' || $arr_uri[2] === 'index.php' ? false : $controlador;
	}

	/**
	 * Establece las cabeceras necesarias
	 * en todos los controlador de la app
	 */
	function establecerCabecerasGenerales()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Allow: GET, POST, OPTIONS, PUT, DELETE");
		header('Content-Type: application/json; charset=utf-8');
	}

	/**
	 * Recibe todos los errores NO CRÍTICOS que se prensentan 
	 * en el sistema y les da formato con la clase Respuesta
	 */
	function errorHandler($errno, $errstr, $errfile, $errline)
	{
		$constantes = get_defined_constants(true)['Core'];
		$errno = obtenerNombreConstanteError($errno);

		$respuesta = new Respuesta([
			'mensaje_tecnico' => $errno . ': ' . $errstr,
		]);

		if(DEBUG == true)
		{
			$respuesta->informacion_adicional = [
				'Archivo' => $errfile, 
				'Línea' => $errline
			];
		}

		Respuesta::http500($respuesta);
		exit(1);
	}

	/**
	 * Recibe todos los errores CRÍTICOS que se prensentan 
	 * en el sistema y les da formato con la clase Respuesta
	 */
	function fatalErrorHandler()
	{
		$error = error_get_last();

	    if( $error !== null) 
	    {
	        $errno = obtenerNombreConstanteError($error["type"]);
	        $archivo = $error["file"];
	        $linea = $error["line"];
	        $errstr = preg_replace("/[\r\n|\n|\r|\t]+/", '',$error['message']);
	        $arr_error = explode('Stack trace:', $errstr);
	        $errstr = $arr_error[0];
	        $pila_llamadas = count($arr_error) > 1 ? explode('#', $arr_error[1]) : null;
	        unset($pila_llamadas[0]);

			$respuesta = new Respuesta([]);

			if(DEBUG == true)
			{
				$respuesta->mensaje_tecnico = $errno . ': ' . $errstr;
				$respuesta->informacion_adicional = [
					'Archivo' => $archivo, 
					'Línea' => $linea
				];
				$respuesta->pila_llamadas = $pila_llamadas;
			}

			Respuesta::http500($respuesta);
			exit(1);
	    }
	}

	/**
	 * Retorna el nombre de una constante de 
	 * error de las definidas en las constantes
	 * del core, recibe el valor de la constante
	 * por el parámetro $errno
	 */
	function obtenerNombreConstanteError($errno)
	{
       	$constantes = get_defined_constants(true)['Core'];
       	$nombre_constante = '';

		foreach ( $constantes as $nombre => $valor )
        {
            if ($valor == $errno)
            {
                $nombre_constante = $nombre;
                break;
            }
        }

        return $nombre_constante;
	}


?>