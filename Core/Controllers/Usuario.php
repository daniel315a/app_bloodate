<?php namespace Controllers;

	class Usuario
	{
		
		public static function get()
		{
			$usuario = new \Models\Usuario();
			$respuesta = \Respuesta::obtenerDefault();

			if(isset($_GET['solicitud']))
			{
				$solicitud = $_GET['solicitud'];

				if($solicitud == 'consultar_donantes')
				{
					$donante = new \Models\Donante();
					$respuesta = $donante->consultarTodos();
				}
			}
			else
			{
				$respuesta = $usuario->consultarUsuarios();
			}

			\Respuesta::http200($respuesta);
		}

		public static function post()
		{
			$usuario = new \Models\Usuario();
			$respuesta = \Respuesta::obtenerDefault();

			if(isset($_POST['solicitud']))
			{
				$solicitud = $_POST['solicitud'];
				
				if($solicitud == 'crear_usuario')
				{

					/*
					 * Información del usuario
					 */
					$usuario->nombre_acceso = $_POST['nombre_acceso'];
					$usuario->setContrasenna($_POST['contrasenna']);
					$usuario->numero_documento = $_POST['numero_documento'];
					$usuario->nombres = $_POST['nombres'];
					$usuario->apellidos = $_POST['apellidos'];
					$usuario->crear();
					$id_usuario = $usuario->getId();

					$tipo_usuario = $_POST['tipo_usuario'];

					if($tipo_usuario == 'donante')
					{
						/*
						 * Información del donante
						 */
						$donante = cast($usuario, '\Models\Donante');
						$donante->setTipoSangre($_POST['id_tipo_sangre']);
						$donante->setIdUsuario($id_usuario);

						$respuesta = $donante->crear();
					}
					else if($tipo_usuario == 'empleado')
					{
						$empleado = cast($usuario, '\Models\Empleado');
						$empleado->telefono = $_POST['telefono'];
						$empleado->setIdUsuario($id_usuario);

						$respuesta = $empleado->crear();
					}
				}
				else if($solicitud == 'login')
				{
					$usuario->nombre_acceso = $_POST['nombre_acceso'];
					$usuario->setContrasenna($_POST['contrasenna']);
					$respuesta = $usuario->login();
				}
			}
			
			\Respuesta::http200($respuesta);
		}

		public static function put()
		{

		}

		public static function delete()
		{

		}

	}

?>